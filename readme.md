# CallHub

### Project Description:
  Callhub

- Technology:
    - Back-End:
         - Python 3.6
         - Django Framework 2.1.

- Backend App - dir: backend/
  - Algorithm in callhub/jarvis/api/views.py
  - Models in callhub/jarvis/models.py

- frontend App - dir: frontend/


### Installation:
- creating virutal environment for the project - using virtualenvwrapper.
  - run the command: `mkvirtualenv callhub`
- activating virtual environment:
    - run the command: `workon callhub`
- installing dependencies:
    - change dir to bacend/
    - run the command from the project folder:
        - `pip install -r requirements/development.txt`
- Source the environment file:
        - `source callhub.env`
- start backend server:
    - run the command: `python manage.py runserver` 
    - default port: http://127.0.0.1:8000/

- start frontend server:
    - run the command: `yarn start` 
    - default port: http://localhost:3000/ 

- go to localhost:3000 to start using the app.

- making api request:
    - make post request to /api/v1/ with n as input to get the nth fibonacci number.
