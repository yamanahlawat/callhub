# Loading Requirements from base.txt
-r base.txt

# Development Server Settings
ipdb==0.11
coverage==4.5.2
factory-boy==2.11.1
Faker==1.0.2