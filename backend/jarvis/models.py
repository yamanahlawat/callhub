from django.db import models

# Create your models here.

class BaseModel(models.Model):
    """
    Generic abstract base class for all other models

    Fields:
    * :attr: `created_date` Creation date
    * :attr: `modified_date` Modified date
    * :attr: `deleted_date` Deletion date
    """
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)
    deleted_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        abstract = True


class NthFibonacciNumber(BaseModel):
    number = models.PositiveIntegerField(help_text='Entered Number')
    nth_fibonacci_number = models.PositiveIntegerField(help_text='Nth Fibonacci Number')

    def __str__(self):
        return f"{self.number}: {self.nth_fibonacci_number}"

    class Meta:
        ordering = ('-created_date',)