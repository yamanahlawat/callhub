# External Imports
from rest_framework.views import APIView
from rest_framework.response import Response

# Internal Imports
from jarvis.models import NthFibonacciNumber
import time

# Django Imports
from django.core.exceptions import ObjectDoesNotExist


class NthFibonacci(APIView):

    def post(self, request):
        """
        Return Nth(n) Fibonacci Number

        Time Complexity:O(n)
        Extra Space: O(1)
        
        Arguments:
            request -- request object
            n {int} -- nth number
        
        Returns:
            [response] -- [return nth fibonacci number]
        """
        start = time.time()
        number = int(request.data.get('n'))
        try:
            n = NthFibonacciNumber.objects.get(number=number).nth_fibonacci_number
            end = time.time()
            return Response({'nth_fibonacci_number': n, 'response_time': (end - start)})
        except ObjectDoesNotExist:
            number_1 = 0
            number_2 = 1
            if number < 0: 
                print("Incorrect input") 
            elif number == 0:
                NthFibonacciNumber.objects.create(number=number, nth_fibonacci_number=number_1)
                end = time.time() 
                return Response({'nth_fibonacci_number': number_1, 'response_time': (end - start)})
            elif number == 1: 
                NthFibonacciNumber.objects.create(number=number, nth_fibonacci_number=number_2)
                end = time.time()
                return Response({'nth_fibonacci_number': number_2, 'response_time': (end - start)})
            else: 
                for i in range(2, number+1): 
                    number_3 = number_1 + number_2
                    number_1 = number_2 
                    number_2 = number_3
                NthFibonacciNumber.objects.create(number=number, nth_fibonacci_number=number_2)
                end = time.time()
                return Response({'nth_fibonacci_number': number_2, 'response_time': (end - start)})