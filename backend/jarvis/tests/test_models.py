# Django Imoprts
from django.test import TestCase

# Internal Imports
from jarvis.models import NthFibonacciNumber
from jarvis.tests.factories import NthFibonacciNumberFactory

# External Imports
from faker import Faker

# Declaration
fake = Faker()

# Create your tests here.
class NthFibonacciNumberTest(TestCase):
    """
    Test Case for NthFibonacciNumber Model
    
    Arguments:
        TestCase 
    """

    def test_number(self):
        dummy_number = fake.random_number(5)
        number = NthFibonacciNumberFactory(number=dummy_number).number
        self.assertEqual(number, dummy_number)

    def test_nth_fibonacci_number(self):
        dummy_nth_fibonacci_number = fake.random_number(5)
        nth_fibonacci_number = NthFibonacciNumberFactory(nth_fibonacci_number=dummy_nth_fibonacci_number).nth_fibonacci_number
        self.assertEqual(dummy_nth_fibonacci_number, nth_fibonacci_number)