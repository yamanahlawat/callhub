# Django Imoprts
from django.test import TestCase, Client

# Internal Imports
from jarvis.models import NthFibonacciNumber
from jarvis.api.views import NthFibonacci
from jarvis.tests.factories import NthFibonacciNumberFactory

# Extenal Imports
from rest_framework.test import APIRequestFactory
from faker import Faker
from rest_framework import status
import json

# Declarations
client = Client()
fake = Faker()


def response_to_json(response):
    str_content = response.content.decode("utf-8")
    return json.loads(str_content)


# Create your tests here.
class NthFibonacciViewTest(TestCase):
    """
    Test Case for NthFibonacci View
    
    Arguments:
        TestCase 
    """

    def setUp(self):
        self.factory = APIRequestFactory()
        self.number = fake.random_number(2)


    def test_create_number(self):
        response = client.post('/api/v1/', data={'n': self.number})
        self.assertEqual(response.status_code, status.HTTP_200_OK)