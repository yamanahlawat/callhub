# Internal Import
from jarvis.models import NthFibonacciNumber

# External Imports
from factory import django, fuzzy, SubFactory

from faker import Faker

fake = Faker()


# Create your tests here.
class NthFibonacciNumberFactory(django.DjangoModelFactory):
    """
    Fake Model Factory for Testing NthFibonacciNumber Model
    
    Arguments:
        django.DjangoModelFactory 
    """

    class Meta:
        model = NthFibonacciNumber

    number = fake.random_number(5)
    nth_fibonacci_number = fake.random_number(5)