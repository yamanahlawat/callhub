from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

# The URL to use when referring to static files (where they will be served from)
STATIC_URL = '/static/'

# Domain allowed to access. avoid using *
ALLOWED_HOSTS = ['*']


# Allowed Origins
CORS_ORIGIN_WHITELIST = (
    'localhost:3000', # ip address of the frontend server
    '13.234.82.167', # ip address of the frontend server
)