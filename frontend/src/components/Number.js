import React, { Component } from 'react';
import axios from 'axios';



class Number extends Component{
    constructor( props ){
        super( props );
        this.state = {
            error: '',
            nthFibonacciNumber: 0,
            responseTime: 0,
        };
        this.handleClick = this.handleClick.bind(this);
        
    }



    handleClick(){

        if (this.props.number < 0 || (this.props.number % 1) !== 0){
            this.setState({
                error: "Enter a valid Number.",
                nthFibonacciNumber: 0,
                responseTime: 0,
            });
        }
        else{
            axios.post('http://13.234.82.167:81/api/v1/', {'n': this.props.number})
                .then(response => {
                        this.setState(
                            {   
                                error: '',
                                nthFibonacciNumber: response.data.nth_fibonacci_number,
                                responseTime: response.data.response_time
                            }
                        );
                    }
                );
        }
    }
    

    render(){
        return (
            <div>
                <div>
                    <button type="submit" onClick={ this.handleClick.bind(this) } className="btn btn-success mt-2 c-hand">
                        get number
                    </button>
                </div>
                <p className="mt-2 btn-success">{ this.state.error }</p>
                <table className="table table-striped table-hover">
                    <thead>
                        <tr>
                        <th>nth fibonnaci number</th>
                        <th>response time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className="active">
                        <td>{ this.state.nthFibonacciNumber }</td>
                        <td>{ this.state.responseTime }</td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        )
    }
}

export default Number;