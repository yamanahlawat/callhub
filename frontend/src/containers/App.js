import React, { Component } from 'react';
import './App.css';
import Aux from '../hoc/Aux';
import Number from '../components/Number';
import 'spectre.css';


class App extends Component {

  constructor( props ){
    super( props );
    this.state = {
        number: 0
    };
    
  }

  handleChange(event) {
    this.setState({number: event.target.value});
  }

  render() {
    return (
      <div className="App d-block">
        <Aux>
          <h2 className="text-primary">Get nth fibonacci number</h2>
          <div className="column col-4 col-mx-auto">
            <div className="form-group mt-2">
              <input className="form-input" type='text' placeholder="Enter Number" onChange={this.handleChange.bind(this)} />
              <Number number={ this.state.number } />
            </div>
          </div>
        </Aux>
      </div>
    );
  }
}

export default App;
